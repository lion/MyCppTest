# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/lion/test/cpp/mysql/jansson/dump.c" "/home/lion/test/cpp/mysql/build/jansson/CMakeFiles/jansson.dir/dump.c.o"
  "/home/lion/test/cpp/mysql/jansson/error.c" "/home/lion/test/cpp/mysql/build/jansson/CMakeFiles/jansson.dir/error.c.o"
  "/home/lion/test/cpp/mysql/jansson/hashtable.c" "/home/lion/test/cpp/mysql/build/jansson/CMakeFiles/jansson.dir/hashtable.c.o"
  "/home/lion/test/cpp/mysql/jansson/hashtable_seed.c" "/home/lion/test/cpp/mysql/build/jansson/CMakeFiles/jansson.dir/hashtable_seed.c.o"
  "/home/lion/test/cpp/mysql/jansson/load.c" "/home/lion/test/cpp/mysql/build/jansson/CMakeFiles/jansson.dir/load.c.o"
  "/home/lion/test/cpp/mysql/jansson/memory.c" "/home/lion/test/cpp/mysql/build/jansson/CMakeFiles/jansson.dir/memory.c.o"
  "/home/lion/test/cpp/mysql/jansson/pack_unpack.c" "/home/lion/test/cpp/mysql/build/jansson/CMakeFiles/jansson.dir/pack_unpack.c.o"
  "/home/lion/test/cpp/mysql/jansson/strbuffer.c" "/home/lion/test/cpp/mysql/build/jansson/CMakeFiles/jansson.dir/strbuffer.c.o"
  "/home/lion/test/cpp/mysql/jansson/strconv.c" "/home/lion/test/cpp/mysql/build/jansson/CMakeFiles/jansson.dir/strconv.c.o"
  "/home/lion/test/cpp/mysql/jansson/utf.c" "/home/lion/test/cpp/mysql/build/jansson/CMakeFiles/jansson.dir/utf.c.o"
  "/home/lion/test/cpp/mysql/jansson/value.c" "/home/lion/test/cpp/mysql/build/jansson/CMakeFiles/jansson.dir/value.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/local/protobuf/include"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/lion/test/cpp/mysql/jansson/json2pb.cc" "/home/lion/test/cpp/mysql/build/jansson/CMakeFiles/jansson.dir/json2pb.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/local/protobuf/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
