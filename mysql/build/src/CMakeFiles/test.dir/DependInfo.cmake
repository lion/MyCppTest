# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/lion/test/cpp/mysql/src/https.cpp" "/home/lion/test/cpp/mysql/build/src/CMakeFiles/test.dir/https.cpp.o"
  "/home/lion/test/cpp/mysql/src/jsontest.cpp" "/home/lion/test/cpp/mysql/build/src/CMakeFiles/test.dir/jsontest.cpp.o"
  "/home/lion/test/cpp/mysql/src/main.cpp" "/home/lion/test/cpp/mysql/build/src/CMakeFiles/test.dir/main.cpp.o"
  "/home/lion/test/cpp/mysql/src/proto.cpp" "/home/lion/test/cpp/mysql/build/src/CMakeFiles/test.dir/proto.cpp.o"
  "/home/lion/test/cpp/mysql/src/test.pb.cc" "/home/lion/test/cpp/mysql/build/src/CMakeFiles/test.dir/test.pb.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/local/protobuf/include"
  "../jansson"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/lion/test/cpp/mysql/build/jansson/CMakeFiles/jansson.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
