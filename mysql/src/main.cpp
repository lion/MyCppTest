
#include <iostream>
#include <functional> 
#include <map>
#include "https.h"
#include <jansson.h>
#include <assert.h>
#include "proto.h"
#include "jsontest.h"

//#include <mysqlx/xapi.h>

class StringSwitch
{
    public:
    using hash_t = size_t;
    static constexpr hash_t prime = 0x100000001B3ull;
    static constexpr hash_t basis = 0xCBF29CE484222325ull;

    static hash_t hash_run_time(const char* str) 
    {
        hash_t ret = basis;

        while (*str) {
            ret ^= *str;
            ret *= prime;
            str++;
        }

        return ret;
    }
    static constexpr hash_t hash_compile_time(const char* str, hash_t last_value = basis) {
        return *str ? hash_compile_time(str + 1, (*str ^ last_value) * prime) : last_value;
    }
};
static constexpr StringSwitch::hash_t operator "" _hash(const char* p, size_t) {
    return StringSwitch::hash_compile_time(p);
}

void simple_switch(const char* str) {
    switch (StringSwitch::hash_run_time(str)) {
        case "first"_hash:
            std::cout << "1st" << std::endl;
            break;
        case "second"_hash:
            std::cout << "2nd" << std::endl;
            break;
        case "third"_hash:
            std::cout << "3rd" << std::endl;
            break;
        default:
            std::cout << "Default..." << std::endl;
    }
}



void Test(Proto* on)
{
    auto m2 = []{
        printf("111111111fffffffffffffffffffffffffffffffuuuckddddd\n");
        };

    std::string ab("hello world,fuck you and you");
    auto m1 = [ab,on](std::vector<int> a){
        printf("vector test :\t%d %s\n",a.size(),ab.c_str());
        on->num = 1100;
    };
    auto m = [ab,on](int a){
        printf("int a test:\t%d %s\n",a,ab.c_str());
        on->num = 1100;
    };

    on->SetLambda(m2);
    on->SetLambdaTwo("1111",m1,int(),std::vector<int>());
    on->SetLambdaTwo("1111",m,int(),int());
    printf("m2:%p\n",&m2);
    char buf[1024];
    sprintf(buf,"%p",&m2);
    on->SetLambda(buf);

}

int main()
{
    for(int i = 0; i < 100; i++)
    {
        int num =  ((i <<1)|1);
        int num2 = ((i <<1)|0);  
        std::cout<<"i="<<i<<" "<<num <<" "<<num2 <<std::endl;
    }
    simple_switch("firstsecond");

    Https one;
    std::string  strResponse;
    one.HttpPost("http://192.168.83.55:7034/login","111",strResponse,30);
    std::cout<<strResponse<<std::endl;
    json_error_t error;
    //json_t用于引用任何JSON节点
    json_t *root = json_loads( strResponse.c_str(), 0, &error );
    if ( !root ) 
    {
        std::cout<< "error: on line" <<error.line<<":\n" << error.text <<std::endl;
        return 1;
    } 
    else 
    {
        // json_is_*宏用于判断数据类型
        // 处理JSON对象
        assert( json_is_object( root ));
        //{"code": 2000, "data": {"id": 129030, "login": "", "token": "307fc442da0ab253a141569332138e2d" ,"loginip"},  
        json_t *code, *children;
        code = json_object_get( root, "code" );
        std::cout<<" code: %d\n"<< json_integer_value( code )<<std::endl;;
        children = json_object_get( root, "data" );
        json_t* pf = json_object_get( children, "pf" );
        json_t* login = json_object_get( children, "login" );
        json_t* token = json_object_get( children, "token" );
        json_t* effect_time = json_object_get( children, "effect_time" );
        json_t* login_ip = json_object_get( children, "login_ip" );
        json_t* login_port = json_object_get( children, "login_port" );
        std::cout<<"pf:" << json_string_value(pf )
            <<"login: "<<json_string_value(login)
            <<"token: "<<json_string_value(token)
            <<"login_ip: "<<json_string_value(login_ip)
            <<"effect_time: "<<json_integer_value(effect_time)
            <<"login_port: "<<json_integer_value(login_port)<<std::endl;


        json_decref( root );
    }
    Proto::Show();
    Proto* ones = new Proto();
    Test(ones) ;
    auto f1 = std::bind(&Proto::Do ,ones);
    std::function<void(void)> f2 = f1;
    f2();
    printf("after:%s num:%d\n",ones->addrs.c_str(),ones->num);
    TestJson::Test();
    return 0;
}
