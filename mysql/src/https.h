/**
 * @brief http 测试
 * @auther aolei1024@gmail.com
 * @date 2019-06-28
 */

#ifndef _HTTPS_H_ 
#define _HTTPS_H_


#include <curl/curl.h>
#include <string>
#include <sstream>
#include <memory.h>

class Https
{
    public:
        Https();
        ~Https();
        void Show(std::string url);
        CURLcode HttpPost(const std::string & strUrl, std::string szJson,std::string & strResponse,int nTimeout);
        static size_t receive_data(void *contents, size_t size, size_t nmemb, void *stream);
    private:
        CURL *curl;
};



#endif //_HTTPS_H_
