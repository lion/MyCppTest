/**
 * @brief proto测试
 * @auther aolei1024@gmail.com
 * @date 2019-07-02
 */
#ifndef _PROTO_H_
#define _PROTO_H_

#include "test.pb.h"
#include <string>
#include <functional>
#include <vector>
#include <type_traits>

class Base
{
    public:
    virtual void operator()()
    {
    }
};
template <bool c,typename T,typename Y>
class Fun :public Base
{
};
template<typename T,typename Y>
class Fun<true,T,Y> :public Base
{
    public:
        Fun(Y fun):addr(fun)
        {

        }
        void operator()()
        {
            T one;
            addr(one); 
        }
    private:
        Y addr;
};
template<class T,typename Y >
class Fun<false,T,Y> :public Base
{
    public:
        Fun(Y fun):addr(fun)
        {

        }
        void operator()()
        {
            std::vector<T> data;
            T one;
            data.push_back(one);
            addr(data); 
        }
    private:
        Y addr;
};

class Proto
{
    public:
        static void Show();

        void SetLambda(std::string addr);
        void SetLambda(std::function<void (void)> cb_);
        void Do();

        template<typename T,typename Y,typename Z>
        void SetLambdaTwo( std::string ,Y fun,T msg ,Z)
        {
            std::shared_ptr<Base> cbs;
            cbs.reset(new Fun<std::is_same<T, Z>::value,T,Y>(fun));
            cb2.push_back(cbs);
        }
        std::vector< std::shared_ptr<Base>> cb2;

        std::function<void (void)> cb;
        std::string addrs;
        int num = 0;
};



#endif//

