
#include "proto.h"
#include <iostream>
#include <stdio.h>


void Proto::Show()
{
    config::Show msg;
    for(int i = 0; i < 10;i++)
    {
        msg.add_intshow(i);
    }
    config::Vector2* v2 = msg.mutable_v2show();
    v2->set_x(1.0f);
    v2->set_y(2.0f);
    config::Vector3* v3 = msg.mutable_v3show();
    v3->set_x(1.0f);
    v3->set_y(2.0f);
    v3->set_z(3.0f);
    for(int i = 0; i < 3;i++)
    {
        config::Vector3* v3r =  msg.add_v3rshow();
        v3r->set_x(i * 1.0f);
        v3r->set_y(i * 2.0f);
        v3r->set_z(i * 3.0f);
    }
    for(int i = 0; i < 3;i++)
    {
        msg.add_sshow("hello world！你好");
    }
    printf("OnHelloWorld \n%s\n",msg.DebugString().c_str());
}




void Proto::SetLambda(std::string addr)
{
    addrs = addr;
}
void Proto::SetLambda(std::function<void (void)> cb_)
{
    cb = cb_;
}
void Proto::Do()
{
    printf("cb:%p\n",&cb);
    cb();
    for(auto it = cb2.begin();it != cb2.end();it++)
    {
        (*(*it))();
    }
}

