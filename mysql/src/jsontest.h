#ifndef _JSON_TEST_H_
#define _JSON_TEST_H_

#include <jansson.h>



class TestJson
{
    public:
        static void Test();
        static bool Show(json_t *object ,int deep = 0);
        static void get_object(json_t *object ,int deep = 0);
};





#endif //_JSON_TEST_H_
