#include <iostream>
#include <functional>
#include <memory>
#include <typeinfo>

class Base
{
    public:
    virtual void Do()=0;
};

template<typename T>
class Test : public Base
{
    public:
        Test(T t):func(t){}
        void Do()
        {
            //std::cout<<"fffff:"<<func.target_type().name()<<std::endl;

           // auto f = std::bind(func);
           // f();
        }
    private:
        T func;
};
#define Name "fffffuuuck"

class TTest
{
    public:
        template<typename T>
        void SetFun(T fun)
        {
            f.reset(new Test<T>(fun));
        }
        void Do()
        {
            f->Do();
        }
        static constexpr const char* svc_name = Name; 
    private:
        std::shared_ptr<Base> f;
};

void Echo(int a)
{
    std::cout<<"11111111\tssss\t"<<a<<std::endl;
}

int main()
{
    typedef std::function<void(int,int)> FunCB;

    TTest one;
    one.SetFun(&Echo);
    one.Do();
    std::cout<<TTest::svc_name<<std::endl;
    return 0;
}
