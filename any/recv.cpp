#include <iostream>
#include <string>



struct Object
{
    Object()
    {
        std::cout<<"Object::Object()"<<std::endl;
    }
    Object(const Object&)
    {
        std::cout<<"Object::Object(const Object&)"<<std::endl;
    }
    ~Object()
    {
        std::cout<<"Object::~object()"<<std::endl;
    }
};

struct Test
{
    Test()
    {
        obj = new Object();

    }
    Test(const Test& A)
    {
        obj = A.obj;
    }
    ~Test()
    {
        delete obj;
    }
    const Test& operator=(Test A)
    {
        this->obj = A.obj;
        return *this;
    }
    Object* obj;
};

void func(const Test& t)
{
//    Test b = A;
}

int main()
{
    Test t;
    std::string  a = "1111";
    std::string  b = a;
    if(a.c_str() == b.c_str())
        std::cout<<"a = b"<<std::endl;

    std::cout<<(void*)&a<<"\t"<<(void*)a.data()<<"\n"<<(void*)&b<<"\t"<<(void*)b.data()<<std::endl;
    std::string  c = "1111112222223333";
    if(a.data() != c.data())
        std::cout<<"a != c"<<std::endl;


    func(t);
    return 0;
}
