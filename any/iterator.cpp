#include <iostream>
#include <map>
#include <list>



void func(std::list<int>::iterator& beg,
          std::map<int,std::list<int>>::iterator v)
{
    auto it = beg;
    int count = 0;
    for(; it != v->second.end();it++)
    {
        count++;
        if(count >= 11)
            break;
    }
    for(auto its = beg; its != it;)
    {
       its =  v->second.erase(its);

    }
    beg = it;
}

int main()
{
    std::map<int, std::list<int>> v;
    std::list<int> l;
    for(int i = 1; i < 10;i++)
    {
        l.push_back(i);
    }
    v.emplace(1,l);
    v.emplace(2,l);
    v.emplace(3,l);
    v.emplace(4,l);
    
    auto it = v.find(2);

    auto iter = it->second.begin();

    func(iter,it);
    for(auto its = iter;its != it->second.end();its++)
    {
        std::cout<<*its<<std::endl;
    }
    std::cout<<"--------------------------------------"<<std::endl;
    for(auto its : it->second)
    {
        std::cout<<its<<std::endl;
    }

    return 0;
}

