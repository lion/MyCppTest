
#include <iostream>
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <string>
#include <sstream>

struct Hello
{
};

#define Game(id) "watch_"#id


void Test(const char* buf)
{
    char* b = "watch_1";
    if(strcmp(b,buf) == 0)
    {
        std::cout<<"Ok"<<std::endl;
    }
}
template<typename T>
void fun(const T& t)
{
    std::cout<<t<<std::endl;
}

template <typename T,typename ... Args>
void fun(const T& t,Args ... args)
{
    std::cout<<t<<"\t";
    fun(args...);
}

template <typename T>
typename std::decay<T> unpack(const T& t)
{
        std::cout << t << ' ';
            typename std::decay<T> _ins;
                return _ins;
}

template <typename T, typename ... Args>
void debugLogImpl(const T& t, const Args& ... args)
{
        std::cout <<"log show:" <<t << ' ';
            auto _fun = [](...) {};
                _fun(unpack(args)...);
                    std::cout << '\n';

    std::cout<<__PRETTY_FUNCTION__<<std::endl;
}

int main()
{
    Hello one;
    std::cout<<"ssss"<<std::endl;
    std::cout<<Game(1)<<std::endl;
    printf("%s\n",Game(1));

    Test(Game(1));
    char buf[1024];
    sprintf(buf,"./log/%s_%s","name","%datetime{%Y%M%d%H}.long");
    std::cout<<buf<<std::endl;

    fun("ssss","222",3333,44444,1.6,'a');
    debugLogImpl(1,'2',"33",444.444,true);

    std::string sty = "\n\f00023";

    std::stringstream vv;
    vv << "'"<<sty<<"'";

    std::cout<<vv.str().c_str()<<std::endl;
    char buffer[1024];
    sprintf(buffer,"'%s'", vv.str().c_str());
    std::cout<<buffer<<std::endl;




    return 0;
    
}

