/*
 * @brief 事件的基类
 * @auther aolei1024@gmail.com
 * @date 2020-05-07
 */
#ifndef _EVENTBASE_H_
#define _EVENTBASE_H_

namespace BT
{

    class EventBase
    {
        public:
            virtual bool Do() = 0;
    };
};


#endif //_EVENTBASE_H_
