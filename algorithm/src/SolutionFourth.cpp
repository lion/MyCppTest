#include "Solution.h"
#include <iostream>
using namespace std;
//45. 跳跃游戏 II
int Solution::jump(vector<int>& nums)
{
	int len = nums.size();
	int step = 0;
	int cur = 0;
	while (cur < len -1)
	{
		int num = nums[cur];
		if (num == 0)
			return 0;
		int maxNext = 0;
		int maxIndex = -1;
		for (int i = 1; i <= num; i++)
		{
			int next = cur + i;
			if (next >= len - 1)
			{
				maxIndex = next;
				break;
			}
			if ( nums[next]+ next >= maxNext)
			{
				maxNext = nums[next] + next;
				maxIndex = next;
			}
		}
		cur = maxIndex;
		cout << "next:" << cur << endl;
		step++;
	}
	return step;
}
vector<vector<int>> per;
vector<int> single;

void combination(vector<int>& nums ,int len)
{
    if(len == 0)
    {
        per.push_back(single);
        return;
    }
	for (int i = 0; i < len; i++)
	{
		if ((i < len-1 && nums[i] == nums[i + 1]) 
			|| ( i != len-1 && nums[i] == nums[len-1]))
			continue;
		single.push_back(nums[i]);
		swap(nums[i], nums[len - 1]);
		combination(nums, len - 1);
		swap(nums[i], nums[len - 1]);
		single.pop_back();
	}
}
void combination1(vector<int>& nums ,int len)
{
    if(len == nums.size())
    {
        per.push_back(nums);
        return;
    }
    combination1(nums,len+1);
	for (int i = 0; i < len; i++)
	{
        if(nums[i] == nums[len])
            break;
		swap(nums[i], nums[len ]);
		combination1(nums, len +1);
		swap(nums[i], nums[len ]);
	}
}


vector<vector<int>> Solution::permute(vector<int>& nums)
{
	combination(nums, nums.size());
	return per;
}
vector<vector<int>> Solution::permuteUnique(vector<int>& nums)
{
	sort(nums.begin(), nums.end());
	combination1(nums, 0);
	return per;
}
void Solution::rotate(vector<vector<int>>& matrix)
{
    int n = matrix.size();
    for(int i = 0; i < n-1;i++)
    {
        for(int j = i; j < n-1-i;j++)
        {
            int x = n-1-i;
            int y = j;
            int temp = matrix[i][j];
            while(1)
            {
                swap(temp,matrix[y][x]);
                if(x== j && y == i)
                    break;
                int tmpx = x;
                x = n-1-y;
                y = tmpx;
            }
        }
    }
}
vector<vector<string>> Solution::groupAnagrams(vector<string>& strs)
{
    unordered_map<double,vector<string>> list;
    int  a[26]={2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97,101};
    for(auto s : strs)
    {
        double t = 1;
        for(char c : s)
            t *= a[c - 'a'];
        list[t].push_back(s);
    }
    vector<vector<string>> res;
    for(auto v : list)
    {
        res.push_back(v.second);
    }
    return res;
}

//vector<string> qlayout;
vector<int> qlayout;
vector<vector<string>> qRes;
bool Queens(int x,int n)
{
    if(x == n)
    {
        int i = 0;
        cout<<"------------"<<endl;
        for(auto v: qlayout)
        {
            if(i >= n)
            {
                cout<<endl;
                i = 0;
            }
            if(v >= 999999)
                cout<<"Q"<<" ";
            else 
                cout<<"."<<" ";
            i++;
        }
        cout<<endl;
        cout<<"------------"<<endl;
        return true;
    }
    for(int i = 0 ; i < n;i++)
    {
        if(qlayout[x *n + i] != 0)
            continue;
        //竖轴全为空
        for(int j = 0;j < n;j++ )
        {
            qlayout[x *n + j] += 1;
        }
        //横轴
        for(int j = 0;j < n;j++ )
        {
            qlayout[j *n + i] += 1;
        }
        //右斜
        for(int j = 1;j< n-i;j++)
        {
            if(x+j < n)
                qlayout[(x+j) *n + i+j] += 1;
            if(x-j >= 0)
                qlayout[(x-j) *n + i+j] += 1;
        }
        //左斜
        for(int j = 1;j <= i;j++)
        {
            if(x+j < n)
                qlayout[(x+j) *n + i-j] += 1;
            if(x-j >= 0)
                qlayout[(x-j) *n + i-j] += 1;
        }
        qlayout[x *n + i] += 999999;
        Queens(x+1,n);
        qlayout[x *n + i] -= 999999;
        for(int j = 0;j < n;j++ )
        {
            qlayout[x *n + j] -= 1;
        }
        //横轴
        for(int j = 0;j < n;j++ )
        {
            qlayout[j *n + i] -= 1;
        }
        //右斜
        for(int j = 1;j< n-i;j++)
        {
            if(x+j < n)
                qlayout[(x+j) *n + i+j] -= 1;
            if(x-j >= 0)
                qlayout[(x-j) *n + i+j] -= 1;
        }
        //左斜
        for(int j = 1;j <= i;j++)
        {
            if(x+j < n)
                qlayout[(x+j) *n + i-j] -= 1;
            if(x-j >= 0)
                qlayout[(x-j) *n + i-j] -= 1;
        }

    }
    return false;
}
vector<vector<string>> Solution::solveNQueens(int n)
{
    qlayout.resize(n*n);
    Queens(0,n);
    return qRes;
}
int Solution::maxSubArray(vector<int>& nums)
{
    int pre = 0, maxAns = nums[0];
    for (const auto &x: nums) {
        pre = max(pre + x, x);
        maxAns = max(maxAns, pre);
    }
    return maxAns;
}
vector<int> Solution::spiralOrder(vector<vector<int>>& matrix) 
{
    vector <int> ans;
    if(matrix.empty()) return ans; //若数组为空，直接返回答案
    int u = 0; //赋值上下左右边界
    int d = matrix.size() - 1;
    int l = 0;
    int r = matrix[0].size() - 1;
    while(true)
    {
        for(int i = l; i <= r; ++i) ans.push_back(matrix[u][i]); //向右移动直到最右
        if(++ u > d) break; //重新设定上边界，若上边界大于下边界，则遍历遍历完成，下同
        for(int i = u; i <= d; ++i) ans.push_back(matrix[i][r]); //向下
        if(-- r < l) break; //重新设定有边界
        for(int i = r; i >= l; --i) ans.push_back(matrix[d][i]); //向左
        if(-- d < u) break; //重新设定下边界
        for(int i = d; i >= u; --i) ans.push_back(matrix[i][l]); //向上
        if(++ l > r) break; //重新设定左边界
    }
    return ans;
}

vector<vector<int>> Solution::merge(vector<vector<int>>& intervals,vector<int>& newInterval)
{
    vector<vector<int>> ans;
    bool inser = false;
    for(int i = 0 ; i < intervals.size();i++)
    {
        if(newInterval[0] > intervals[i][1])
        {
            ans.push_back(intervals[i]);
            continue;
        }
        if(newInterval[1] < intervals[i][0])
        {
            if(!inser)
            {
                ans.push_back(newInterval);
                inser = true;
            }
            ans.push_back(intervals[i]);
            continue;
        }
        newInterval[0] = min(newInterval[0],intervals[i][0]);
        newInterval[1] = max(newInterval[1],intervals[i][1]);
    }
    if(!inser)
        ans.push_back(newInterval);
    return  ans;
}
int Solution::uniquePaths(int m, int n)
{
	if (m == 0 || n == 0)
		return 0;
	if (m == 1 || n == 1)
		return 1;
	int v[m][n];
	v[0][0] = 1;
	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < n; j++)
		{
			if (i <= 0 || j <= 0)
				v[i][j] = 1;
			else
			{
				v[i][j] = v[i - 1][j] + v[i][j - 1];
			}
		}
	}
	return v[m - 1][n - 1];
}

vector<vector<string>> ret;
unordered_map<string,bool> flag;
struct Node
{
    Node(string s):str(s){}
    Node* parents = nullptr;
    unordered_map<string, Node*> child;
    string str;
};


bool checkWord(string a,string b)
{
    int find = -1;
    for(int i = 0; i < b.length();i++)
    {
        if(a[i] != b[i])
        {
            if(find >= 0)
            {
                find = -1;
                break;
            }
            find = i;
        }
    }
    return find >= 0;
}

vector<vector<string>> Solution::findLadders(string beginWord, string endWord, vector<string>& wordList) 
{
    unordered_map<string, Node*> orphan;
    Node* head = new Node(beginWord);
    for(auto s: wordList)
    {
        auto node = new Node(s);
       if(checkWord(beginWord,s))
       {
           head->child[s] = node;
           node->parents = head;
       }
       else
       {
           for(auto it : head->child)
           {
               if(checkWord(it.first,s))
               {
                   it.second->child[s] = node;
                   node->parents = it.second;
               }
           }
       }
    }
    return ret;
}
bool isOk(string s)
{
    int i = 0;
    int j = s.length() -1;
    while(i<=j)
    {
        if(s[i] != s[j])    
            return false;
        i++;
        j--;
    }
    return true;
}



vector<vector<string>> Solution::partition(string s) 
{
    queue<>
    return {};
}

