/*
 * @brief
 * @auther aolei1024@gmail.com
 * @date 2020-07-01
 */
#ifndef _SOLUTION_H_
#define _SOLUTION_H_

#include <vector>
#include <string>
#include <unordered_map>
#include<bits/stdc++.h> 


using namespace std;


struct ListNode 
{
	int val;
	ListNode *next;
	ListNode(int x) : val(x), next(NULL) {}
};
class Solution 
{
    public:
        static void Test();
    public:
        static int threeSumClosest(vector<int>& nums, int target);
        static vector<vector<int>> fourSum(vector<int>& nums, int target);
        static vector<string> letterCombinations(string digits);
		static ListNode* removeNthFromEnd(ListNode* head, int n);
		static bool isValid(string s);
        static ListNode* mergeTwoLists(ListNode* l1, ListNode* l2);
        static  vector<string> generateParenthesis(int n);
        static ListNode* swapPairs(ListNode* head);
        static ListNode* reverseKGroup(ListNode* head, int k);
        static ListNode* mergeKLists(vector<ListNode*>& lists);
		static int removeDuplicates(vector<int>& nums);
		static int removeElement(vector<int>& nums, int val);
		static int strStr(string haystack, string needle);
		static int divide(int dividend, int divisor);
		static vector<int> findSubstring(string s, vector<string>& words);
        static void nextPermutation(vector<int>& nums);
        static int longestValidParentheses(string s);
        static int search(vector<int>& nums, int target);
        static int searchInsert(vector<int>& nums, int target);
        static vector<int> searchRange(vector<int>& nums, int target);
        static bool isValidSudoku(vector<vector<char>>& board);
        static void solveSudoku(vector<vector<char>>& board);
        static vector<vector<int>> combinationSum(vector<int>& candidates, int target);
		static string countAndSay(int n);
        static vector<vector<int>> combinationSum2(vector<int>& candidates, int target);
        static int trap(vector<int>& height);
        static int firstMissingPositive(vector<int>& nums);
        static string multiply(string num1, string num2);
        static bool isMatch(string s, string p);
		static int jump(vector<int>& nums);
        static vector<vector<int>> permute(vector<int>& nums);
		static vector<vector<int>> permuteUnique(vector<int>& nums);
        static void rotate(vector<vector<int>>& matrix);
        static vector<vector<string>> groupAnagrams(vector<string>& strs);
        static vector<vector<string>> solveNQueens(int n);
		static int maxSubArray(vector<int>& nums);
		static int uniquePaths(int m, int n);
        static vector<int> spiralOrder(vector<vector<int>>& matrix) ;
        static vector<vector<int>> merge(vector<vector<int>>& intervals,vector<int>& newInterval);
        static vector<vector<string>> findLadders(string beginWord, string endWord, vector<string>& wordList);
        static vector<vector<string>> partition(string s);
};


#endif //_SOLUTION_H_
