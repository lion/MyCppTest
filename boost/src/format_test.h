/*
 * @brief 手动实现一个boost库的format
 * @auther aolei1024@gmail.com
 * @date 2019-12-10
 */
#ifndef _FORMAT_TEST_H_
#define _FORMAT_TEST_H_
#include <string>
#include <vector>

class FormatTest
{
    public:
       
        struct FormatNode
        {
            int index = 0;
            std::string str;
        };
        FormatTest(std::string);
        FormatTest& operator%(const int& arg)
        {
            char buf[1024];
            sprintf(buf,"%d",arg);
            param.push_back(buf);
            return *this;
        }
         FormatTest& operator%(const long& arg)
        {
            char buf[1024];
            sprintf(buf,"%ld",arg);
            param.push_back(buf);
            return *this;
        }
          FormatTest& operator%(const float& arg)
        {
            char buf[1024];
            sprintf(buf,"%f",arg);
            param.push_back(buf);
            return *this;
        }
           FormatTest& operator%(const double& arg)
        {
            char buf[1024];
            sprintf(buf,"%f",arg);
            param.push_back(buf);
            return *this;
        }    
          FormatTest& operator%(const char* arg)
        {
            param.push_back(arg);
            return *this;
        }

          FormatTest& operator%(const std::string& arg)
        {
            param.push_back(arg);
            return *this;
        }
        std::string Str()
        {
            std::string ret;
            ret.append(prefix);
            for(auto v : items)
            {
                ret.append(param[v.index-1]);           //TODO:只是为了测试流程，生产环境这里需要做检测越界
                ret.append(v.str);
            }
            return ret;
        }
        void parsing(std::string);
          
        static void Do();
    private:
        std::string prefix;
        std::vector<FormatNode> items;
        std::vector<std::string> param;         //参数
};


#endif //_FORMAT_TEST_H_
