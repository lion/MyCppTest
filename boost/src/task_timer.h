/*
 * @brief
 * @auther aolei1024@gmail.com
 * @date 2019-12-04
 */
#ifndef _TASK_TIMER_H_
#define _TASK_TIMER_H_
#include <boost/asio/io_service.hpp>
#include <boost/asio/deadline_timer.hpp>
#include <boost/system/error_code.hpp>
#include <boost/make_shared.hpp>
#include <iostream>
#include "task_test.h"

namespace detail
{
    typedef boost::asio::deadline_timer::duration_type duration_type;
    template <class Functor>
    class timer_task : public task_wrapped<Functor>
    {
        typedef task_wrapped<Functor> base_t;
        boost::shared_ptr<boost::asio::deadline_timer> timer_;
        public:
        template<class Time>
            explicit timer_task(
                boost::asio::io_service& ios,
                const Time& duration_or_time,
                const Functor& task_wrapped)
            :base_t(task_wrapped)
             ,timer_(boost::make_shared<boost::asio::deadline_timer>(boost::ref(ios),duration_or_time))
        {
        }
        void push_back() const
        {
            timer_->async_wait(*this);
        }
        void operator()(const boost::system::error_code& error) const
        {
            if(! error)
            {
                base_t::operator()();
            }
            else
            {
                std::cerr<<error<<std::endl;
            }
        }
    };
};


#endif //_TASK_TIMER_H_
