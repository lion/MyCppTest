#include <iostream>
#include <fstream>
#include <string>
#include <sys/time.h>
#include <vector>
#include <boost/random.hpp>
#include <boost/random/random_device.hpp>
#include <boost/progress.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/any.hpp>
#include <boost/foreach.hpp>
#include <boost/assign.hpp>
#include <locale>
#include <boost/program_options.hpp>
#include "io_server_test.h"
#include "format_test.h"
#include <boost/config.hpp>

namespace po = boost::program_options;

boost::random::mt19937 gen;
int main( int argc ,char* argv[])
{



    FormatTest::Do();
    po::options_description desc("Allowed options");
    desc.add_options()
        ("help", "produce help message")
        ("help,h", "produce help message")
        ("compression", po::value<int>(), "set compression level");
    //("compression", po::value<int>(&level)->default_value(1), "set compression level");

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if(vm.count("help"))
    {
        std::cout<<desc<<std::endl;
    }

    if(vm.count("compression"))
    {
        std::cout<<"compression level was set to "<<vm["compression"].as<int>()<<"."<<std::endl;
        //cout<<"compression level: "<<level<<endl;
    }
    else
    {
        std::cout<<"compression level was not set."<<std::endl;
    }


    std::cout<<"hello boost"<<std::endl;
    boost::progress_timer t;


    boost::gregorian::date d(2010, 1, 30); 
    std::cout << d.year() << std::endl; 
    std::cout << d.month() << std::endl; 
    std::cout << d.day() << std::endl; 
    std::cout << d.day_of_week() << std::endl; 
    std::cout << d.end_of_month() << std::endl; 

    std::vector<boost::any> m;
    m.push_back(2);
    m.push_back(std::string("111"));
    m.push_back(1.2f);
    BOOST_FOREACH(boost::any i, m )
    {
        std::cout<<i.type().name()<<std::endl;
        try
        {
            int  res = boost::any_cast<int>(i);
            std::cout<<"i="<<res<<std::endl;
        }
        catch(boost::bad_any_cast & ex)
        {
            std::cout<<"cast error"<<ex.what()<<std::endl;
        }
    }
    struct timeval tv;
    gettimeofday(&tv,NULL);
    std::cout<<"milliseconds:"<<tv.tv_sec * 1000 + tv.tv_usec/1000<<std::endl;
    
    using namespace boost::assign;
    std::vector<int > vs;
    vs += 1,2,3,4,5;
    vs += 1,2,3,4,5,6;

    BOOST_FOREACH(int i,vs)
    {
        std::cout<<"i=\t"<<i<<std::endl;
    }

    std::vector<std::string> v(10);
    std::ofstream fs("test");
    boost::progress_display pd(v.size());
    std::vector<std::string>::iterator pos;

    std::locale loc;
    std::cout<<loc.name()<<std::endl;

    for(pos=v.begin();pos!=v.end();++pos)
    {
        fs<<*pos<<std::endl;
        ++pd;
        sleep(1);
    }

    

    boost::uniform_int<> real(0,999);
    for(int i = 0; i < 1000;i++)
    {
        std::cout<<real(gen)<<std::endl;
    }


    IOServerTest one;
    one.Run();
    return 0;
}
