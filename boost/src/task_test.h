/*
 * @brief 测试任务分发
 * @auther aolei1024@gmail.com
 * @date 2019-12-04
 */
#ifndef _TASK_TEST_H_
#define _TASK_TEST_H_

#include <boost/thread/thread.hpp>

#include <iostream>

namespace detail
{
    template<class T>
    class task_wrapped
    {
        private:
            T task_unwrapped_;
        public:
            explicit task_wrapped(const T& task_unwrapped)
                : task_unwrapped_(task_unwrapped)
            {
            }
            void operator()()const
            {
                try
                {
                    boost::this_thread::interruption_point();
                }
                catch(const boost::thread_interrupted&)
                {
                }
                try
                {
                    task_unwrapped_();
                }
                catch(const std::exception& e)
                {
                    std::cerr<<"Exception: "<< e.what() <<std::endl;
                }
                catch(const boost::thread_interrupted&)
                {
                    std::cerr <<"Thread interrupted"<<std::endl;
                }
                catch(...)
                {
                    std::cerr<<"Unknown exception"<<std::endl;
                }
            }
            static task_wrapped<T> make_task_wrapped(const T& task_unwrapped)
            {
                return task_wrapped<T>(task_unwrapped);
            }
    };


};

#endif //_TASK_TEST_H_
