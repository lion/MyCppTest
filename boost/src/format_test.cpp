#include "format_test.h"
#include <boost/format.hpp>
#include <iostream>
#include <string>




struct  TmpObject
{
    TmpObject(const std::string str):member(str)
    {

    }
    void operator%(const int id)
    {
        std::cout<<member<<id<<std::endl;
    }
    std::string member;
};

FormatTest::FormatTest(std::string str)
{

    parsing(str);
}

void FormatTest::parsing(std::string str)
{
    std::string::size_type i0 = 0,i1 = 0;
    int cur_item = 0;
    while((i1 = str.find("%",i1)) != std::string::npos)
    {
        std::string tmp;
        if(str[i1+1] == str[i1]) //过滤%%的情况
        {
            std::cout<<"tmp:"<<tmp<<std::endl;
            tmp.append(str,i0,i1+1 - i0);
            std::cout<<"tmp:"<<tmp<<"\ti0="<<i0<<"\ti1="<<i1<<std::endl;
            if(cur_item == 0)
            {
               prefix.append(tmp);
            }
            else
            {
                items[cur_item-1].str.append(tmp);
            }
            i1 += 2; i0 = i1;
            continue;
        }
        if(str[i1+2] == str[i1])            //只支持1-9的参数输入
        {
            tmp.append(str,i0,i1 - i0);
            if(cur_item == 0)
            {
               prefix.append(tmp);
            }
            else
            {
                items[cur_item-1].str.append(tmp);
            }
            cur_item++;
            int n = str[i1+1] - '0'; 
            FormatNode tmps;
            tmps.index = n;
            items.push_back(tmps);
            i1 += 3; i0 = i1;
            continue;
        }
        else 
        {
            std::cout<<"tmp:"<<tmp<<std::endl;
            tmp.append(str,i0,i1 +1 - i0);
            std::cout<<"tmp:"<<tmp<<"\ti0="<<i0<<"\ti1="<<i1<<std::endl;
            if(cur_item == 0)
            {
               prefix.append(tmp);
            }
            else
            {
                items[cur_item-1].str.append(tmp);
            }
            i1 += 1; i0 = i1;
        }
    }
}

void FormatTest::Do()
{
    std::cout << boost::format{"%3%.%1%.%2% 100%%"} % 12 % 5 % 2014 << "\n"<<boost::format{"%1%.%2%.%3%"} % 12 % 5 % 2014<< std::endl;
    TmpObject("hello world")%10;
    //world.%1%hello.%2% 10029 100%%nihaobuhanihaobuhaogan%
    //world.hello.29 100nihaobuhaogan
    //worldhello29 100%nihaobuhaogan%
    //world.hello.29 100%nihaobuhaogan%
    std::cout<< (FormatTest("hello %3%. and %1% you . I am %2% old ,100%%nihaobuhaogan%")% "hello" % 29 % "world").Str()<<std::endl;


}
