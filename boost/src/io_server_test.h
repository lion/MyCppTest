/*
 * @brief 测试boost的io_server功能用法
 * @auther aolei1024@gmail.com
 * @date 2019-12-02
 */
#ifndef _IO_SERVER_TEST_H_
#define _IO_SERVER_TEST_H_

#include <boost/asio.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/shared_ptr.hpp>

class IOServerTest
{
    public:
        void Run();
        static void DoShow();
        static void Show(int i);
        static void ProducerThread(boost::shared_ptr< boost::asio::io_service > io_service);
        static void WorkerThread( boost::shared_ptr< boost::asio::io_service > io_service );
        static void Print(const boost::system::error_code & ec);
};


#endif //_IO_SERVER_TEST_H_
