/*
 * @brief
 * @auther aolei1024@gmail.com
 * @date 2019-12-04
 */
#ifndef _TASK_PROCCESSOR_H_
#define _TASK_PROCCESSOR_H_
#include <boost/asio/io_service.hpp>
#include <boost/noncopyable.hpp>
#include "task_test.h"

class task_processor : private boost::noncopyable
{
    private:
        boost::asio::io_service ios_;
        boost::asio::io_service::work work_;
        static task_processor* ptr;
        task_processor():ios_(),work_(ios_)
    {

    }
    public:
        static task_processor& get()
        {
            if(ptr == NULL)
                ptr = new task_processor();
            return *ptr;
        }
        template<class T>
            inline void push_task(const T& task_unwrapped)
            {
                ios_.post(detail::task_wrapped<T>::make_task_wrapped(task_unwrapped));
            }
        void start()
        {
            ios_.run();
        }
        void stop()
        {
            ios_.stop();
        }
};
task_processor* task_processor::ptr = NULL;

#endif //_TASK_PROCCESSOR_H_
