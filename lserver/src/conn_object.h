/*
 * @brief 链接对象
 * @auther aolei1024@gmail.com
 * @date 2020-06-29
 */
#ifndef _CONN_OBJECT_H_
#define _CONN_OBJECT_H_

#include "byte_buffer.h"

class ConnObject
{
    public:
        ConnObject();
        ~ConnObject();
        void init();
    private:
       CumBuffer readbuf;
       CumBuffer writebuf;
};


#endif //_CONN_OBJECT_H_
