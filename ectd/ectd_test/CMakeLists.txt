project(etcd_test)
set(project_name etcd_test)

cmake_minimum_required(VERSION 3.10)
set (CMAKE_C_FLAGS_DEBUG "-fpermissive -fopenmp  -std=c++11 -g -fno-elide-constructors " )
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

INCLUDE_DIRECTORIES(
            /usr/local/include
            /usr/local/include/etcd/proto/
            /usr/include/cpprest/
            /usr/local/protobuf/include/
			)
LINK_DIRECTORIES(
			/usr/local/lib/
            /usr/local/protobuf/lib/
            /usr/lib/x86_64-linux-gnu/
			)

SET(EXECUTABLE_OUTPUT_PATH ${CMAKE_SOURCE_DIR}/bin)
add_subdirectory(src)

