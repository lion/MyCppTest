#include <iostream>
#include "etcd/Client.hpp"
#include "etcd/KeepAlive.hpp"
#include <chrono>
#include <thread>

int main()
{
    etcd::Client etcd("http://127.0.0.1:2379");
    etcd::Response rep = etcd.leasegrant(10).get();
    int64_t lease_id = rep.value().lease();
    {
    etcd::Response resp = etcd.set("/test/key1", "44",rep.value().lease()).get();
    std::cout<<"error_code:"<<resp.error_code()<<std::endl;
    }
 {
    etcd::Response resp = etcd.set("/test/key2", "43",rep.value().lease()).get();
    std::cout<<"error_code:"<<resp.error_code()<<std::endl;
    }
 {
    etcd::Response resp = etcd.set("/test/key3", "34",rep.value().lease()).get();
    std::cout<<"error_code:"<<resp.error_code()<<std::endl;
    }
 {
    etcd::Response response = etcd.ls("/test").get();
    for(auto i = 0; i < response.keys().size();i++)
    {
        std::cout<<response.key(i)<<"\t"<<response.value(i).as_string()<<std::endl;
    }
    std::cout << response.value().as_string()<<std::endl;
 }
    
  etcd::KeepAlive keepalive(etcd, 10,lease_id);
    while(1)
    {
        std::this_thread::sleep_for(std::chrono::seconds(1)); 
    }
    return 0;
}

