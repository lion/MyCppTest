#include <iostream>
#include <chrono>
#include <thread>
#include <string>
#include "etcd/Watcher.hpp"

void printResponse(etcd::Response const & resp)
{
    std::cout << "print response called" << std::endl;
    if (resp.error_code()) {
        std::cout << resp.error_code() << ": " << resp.error_message() << std::endl;
    }
    else
    {
        for(auto event : resp.events())
        {
            std::cout<<"\tkey:"<< event.kv().key()
                     <<"\tvalue:"<< event.kv().value()
                     <<std::endl;
        }

        std::cout << resp.action() << " " << resp.value().as_string() << std::endl;
        std::cout << "Previous value: " << resp.prev_value().as_string() << std::endl;
    }
}
int main()
{
    std::string etcd_uri("http://127.0.0.1:2379");

    /*
    etcd::Client client("http://127.0.0.1:2379");
    client.watch("/test",true).then([&](pplx::task<etcd::Response> resp_task)
                                    {
                                        etcd::Response resp = resp_task.get();
                                        for(auto event : resp.events())
                                        {
                                            std::cout<<"\tkey:"<< event.kv().key()
                                                <<"\tvalue:"<< event.kv().value()
                                                <<std::endl;
                                        }
                                        std::cout << resp.action() << " " << resp.value().as_string() << std::endl;
                                        std::cout << "Previous value: " << resp.prev_value().as_string() << std::endl;
                                    });
    std::cout<<"over"<<std::endl;
    */
    etcd::Watcher watcher(etcd_uri, "/test", printResponse, true);
    while(1)
    {

        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
    return 0;
}
