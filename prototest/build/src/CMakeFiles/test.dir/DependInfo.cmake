# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/lion/test/cpp/prototest/src/config.buff.pb.cc" "/home/lion/test/cpp/prototest/build/src/CMakeFiles/test.dir/config.buff.pb.cc.o"
  "/home/lion/test/cpp/prototest/src/main.cpp" "/home/lion/test/cpp/prototest/build/src/CMakeFiles/test.dir/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/local/protobuf/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
