#include <iostream>
#include <string>

#include <google/protobuf/message.h>
#include <google/protobuf/text_format.h>
#include "config.buff.pb.h"

// 这个函数生成的c++代码调用
inline bool readfile2buf(const char* strFileName, std::string& result)
{
    result.clear();

    FILE* file = fopen(strFileName, "rb");
    if (!file) {
        fprintf(stderr, "game_config: cannot open %s\n", strFileName);
        return false;
    }

    fseek(file, 0, SEEK_END);
    unsigned int nFileLen = ftell(file);
    fseek(file, 0, SEEK_SET);

    result.resize(nFileLen);
    size_t n = fread((char*)result.c_str(), nFileLen, 1, file); 
    
    fclose(file);
    return true;
}

int main()
{

    config::buff_table msg; 
    //config::buff* buff = msg.add_records();
    //buff->set_enumtest(config::TEST_TWO);
    //buff->set_id(1);
    std::string strData;
    readfile2buf("buff.cfg", strData);
    google::protobuf::TextFormat::ParseFromString(strData, &msg);
    
    std::string strOut;
    //msg.SerializeToString(&strOut);
    google::protobuf::TextFormat::PrintToString(msg, &strOut);
    std::cout<<":::"<<strOut<<std::endl;
    return 0;
}
