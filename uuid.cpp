#include <iostream>
#include <cmath>

using namespace std;

struct Vector3 {
    double x;
    double y;
    double z;
};

double length(Vector3 a)
{
    return sqrt(a.x*a.x + a.y*a.y + a.z*a.z);
}

int main()
{
    Vector3 a{12, 4, 2};
    double len = a.length();
    cout << len << endl;
    return 0;
}
