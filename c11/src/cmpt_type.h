/*
 * @brief
 * @auther aolei1024@gmail.com
 * @date 2020-06-09
 */
#ifndef _CMPT_TYPE_H_
#define _CMPT_TYPE_H_

#include "type_id.h"


namespace Temp
{
    class CmptType 
    {
        public:
            explicit constexpr CmptType(size_t id) : hashcode{ id } {}
            template<typename Cmpt>
            static constexpr CmptType Of() noexcept { return CmptType{ TypeID<Cmpt> }; }
            constexpr size_t HashCode() const noexcept { return hashcode; }
            template<typename Cmpt>
            static constexpr size_t HashCodeOf() noexcept { return TypeID<Cmpt>; }
            static constexpr CmptType Invalid() noexcept { return CmptType{ static_cast<size_t>(-1) }; }
            template<typename Cmpt>
            bool Is() const noexcept { return hashcode == HashCodeOf<Cmpt>(); }
        private:
            size_t hashcode;
    };
    inline constexpr bool operator<(const CmptType& x, const CmptType& y) noexcept 
    {
        return x.HashCode() < y.HashCode();
    }
    inline constexpr bool operator==(const CmptType& x, const CmptType& y) noexcept 
    {
        return x.HashCode() == y.HashCode();
    }
}


#endif //_CMPT_TYPE_H_
