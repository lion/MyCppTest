/*
 * @brief 
 * @auther aolei1024@gmail.com
 * @date 2019-08-23
 */
#ifndef _MAP_ASSIGN_H_
#define _MAP_ASSIGN_H_
#include <map>
#include <mutex>
#include <condition_variable>
namespace Assign
{
    class Test
    {
        public:
            template<class K,class V>
                std::pair<K,V> operator()(K key,V value)
                {
                    return std::make_pair(key,value);
                }

        static void Func1(Test* ptr);
        static void Func2(Test* ptr);
        void Run();
        private:
        std::mutex lock;
        int i = 0;
        std::condition_variable cv;
    };
};





#endif //_MAP_ASSIGN_H_
