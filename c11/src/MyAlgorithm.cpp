#include "MyAlgorithm.h"
#include <iostream>

void AlgorithmTest::Test()
{
    int num[] = {10,9,8,7,6,5,4,3,2,1,0};
    int len = sizeof(num)/sizeof(int);
    QSort(num,0,len -1);
    for(int i = 0; i < len;i++)
    {
        std::cout<<num[i]<<" ";
    }
    std::cout<<std::endl;
    
}
void AlgorithmTest::QSort( int *num, int beg, int end )
{
    if(beg >= end)
        return;
    int left = beg;
    int right = end -1;
    int mid = num[end];
    while( left < right)
    {
        while(num[left] <= mid && left < right)
            left++;
        while(num[right] >= mid && right > left)
            right--;
        if(num[left] > num[right])
        {
            int tmp = num[left];
            num[left] = num[right];
            num[right] = tmp;
        }
    }
    if(num[left] > num[end])
    {
        int tmp = num[left];
        num[left] = num[end];
        num[end] = tmp;
    }
    else
    {
        left++;
    }
    QSort(num,0,left-1);
    QSort(num,right+1,end);
}
