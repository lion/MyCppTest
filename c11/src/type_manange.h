/*
 * @brief
 * @auther aolei1024@gmail.com
 * @date 2020-06-09
 */
#ifndef _TYPE_MANANGE_H_
#define _TYPE_MANANGE_H_

#include "cmpt_type.h"
#include <unordered_map>

namespace Temp
{
    class TypeMange
    {

        private:
            std::unordered_map<CmptType,size_t> sizeofs;
            std::unordered_map<CmptType,size_t> alignments;
    };
}


#endif //_TYPE_MANANGE_H_
