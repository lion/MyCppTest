
#ifndef _TEMPLATE_TEST_INL_
#define _TEMPLATE_TEST_INL_
#include <iostream>

template<typename System>
void SystemTest::Doone()
{
    System one;
    std::cout<<one.Show()<<std::endl;
}


template<typename... Systems>
void SystemTest::Do()
{
    (Doone<Systems>(),...);
}

#endif //_template_test_inl_
