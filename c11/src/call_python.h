/*
 * @brief C++ 调用Python测试
 * @auther aolei1024@gmail.com
 * @date 2020-06-17
 */
#ifndef _CALL_PYTHON_H_
#define _CALL_PYTHON_H_


#include <string>

class CallPython
{
    public:
        CallPython();
        ~CallPython();
        void CallFunction(std::string func);
        void CallFile(std::string name);
        void Test();
};


#endif //_CALL_PYTHON_H_
