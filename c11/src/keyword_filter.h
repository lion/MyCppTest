/*
 * @brief 关键字过滤
 * @auther aolei1024@gmail.com
 * @date 2020-01-03
 */
#ifndef _KEYWORD_FILTER_H_
#define _KEYWORD_FILTER_H_

#include <string>

struct KeywordNode
{
    unsigned char ch;
    bool end = false;
    int length = 0;
    KeywordNode* nodes[256];
};

class FilterOper
{
    public:
        FilterOper();
        ~FilterOper();
        void Load(std::string file);
        void Show();
        bool DepShow(KeywordNode* node,bool& next,int dep=0);
        bool operator()(const char* str);

        bool find(KeywordNode* node,const char* str);
    private:
    KeywordNode* nodes = nullptr;
};


///---------------------------------------------------第二版本-------------------------------

//第一版和第二版其实效率都一样，但是第二版没有过多浪费内存，且第二版本的可以显示中文
#include <map>
#include <memory>

struct KeywordNode2rd
{
    std::string word;
    bool end = false;
    int length = 0;
    std::map<std::string,std::shared_ptr<KeywordNode2rd>> words;
};

class FilterOper2rd
{
    public:
        void Load(std::string file);
        void Show();
        bool DepShow(std::shared_ptr<KeywordNode2rd> node,bool& next,int dep=0);
        bool operator()(const char* str);
        bool find(std::shared_ptr<KeywordNode2rd> node,const char* str);
    private:
        std::shared_ptr<KeywordNode2rd> nodes = nullptr;

};





#endif //_KEYWORD_FILTER_H_
