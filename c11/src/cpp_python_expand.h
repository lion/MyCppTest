/*
 * @brief C++扩展python
 * @auther aolei1024@gmail.com
 * @date 2020-06-22
 */
#ifndef _CPP_PYTHON_EXPAND_H_
#define _CPP_PYTHON_EXPAND_H_

#include "singlenton.h"
#include <python3.8/Python.h>
class CppPythonExpand :public Singleton<CppPythonExpand>
{
    public:

    void Run();

    void SetPyObject(PyObject* p)
    {
        m = p;
    }
    PyObject* GetPyObject()
    {
        return m;
    }
    private:
    PyObject *m = nullptr;
};


#endif //_CPP_PYTHON_EXPAND_H_
