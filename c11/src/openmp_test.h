/*
 * @brief
 * @auther aolei1024@gmail.com
 * @date 2020-06-16
 */
#ifndef _OPENMP_TEST_H_
#define _OPENMP_TEST_H_

class OpenMp
{
    public:
        static void show();
        void test();
};


#endif //_OPENMP_TEST_H_
