/*
 * @brief 实现一些算法测试
 * @auther aolei1024@gmail.com
 * @date 2020-01-13
 */
#ifndef _MYALGORITHM_H_
#define _MYALGORITHM_H_

class AlgorithmTest
{
    public:
        static void Test();
        static void QSort(int num[],int beg,int end);
};



#endif //_MYALGORITHM_H_
