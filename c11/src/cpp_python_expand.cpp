#include "cpp_python_expand.h"

#include <iostream>
#define APPEND_SCRIPT_MODULE_METHOD(MODULE, NAME, FUNC, FLAGS, SELF)                        \
    static PyMethodDef __pymethod_##NAME = {#NAME, (PyCFunction) FUNC, FLAGS, NULL};        \
    if(PyModule_AddObject(MODULE, #NAME, PyCFunction_New(&__pymethod_##NAME, SELF)) != 0)   \
{                                                                                       \
    printf("append " #NAME " to pyscript error!\n");                                    \
}                                                                                       \




static PyObject* hello_print(PyObject* self,PyObject* args)
{
    PySys_WriteStdout("hello, world and fuck you\n");
    Py_INCREF(Py_None);
     return Py_None;
}
static PyMethodDef hello_methods[] = 
{
    {"printf", (PyCFunction)hello_print, METH_NOARGS, "Print the hello world."},
    { NULL, NULL,0,NULL }
};



void PyInit_hello()
{
    auto m = PyImport_AddModule("hello");
    APPEND_SCRIPT_MODULE_METHOD(m, print, hello_print, METH_VARARGS, 0);
//    PyTypeObject _scriptType;
//    PyMethodDef* _lpScriptmethods = nullptr;
}

void CppPythonExpand::Run()
{
    PyInit_hello();
    std::cout<<__FUNCTION__<<std::endl;

}



