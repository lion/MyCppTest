#include "openmp_test.h"
#include <iostream>
void OpenMp::show()
{
    std::cout<<"----------------begin------------------"<<std::endl;
    for(int i = 0; i < 10;i++)
    {
        std::cout<<i<<std::endl;
    }
    std::cout<<"----------------end------------------"<<std::endl;
}
#pragma omp parallel num_threads(8);
void OpenMp::test()
{
#pragma omp master
    show();
    std::cout<<"threads begin----------------"<<std::endl;
#pragma omp task
    for(int i = 0; i < 8;i++)
        show();
#pragma omp taskwait
    std::cout<<"over"<<std::endl;
}

