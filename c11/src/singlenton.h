/**
 * @brief 单例工厂
 * @auther aolei1024@gmail.com
 * @date 2019-06-19
 */
 
#ifndef _SINGLENTON_H_
#define _SINGLENTON_H_

template<typename T>
class SingletonFactory
{
    public:
        static T* instance()
        {
            return new T();
        }
};

template <typename T,typename MANA = SingletonFactory<T> >
class Singleton 
{
    protected:
        static T* singlenton;
        Singleton(){}
        ~Singleton(){}
    public:
        static void DelMe()
        {
            if(singlenton)
            {
                delete singlenton;
                singlenton = nullptr;
            }
        }
        static T* Instance()
        {
            if(!singlenton)
            {
                singlenton = MANA::instance();
            }
            return singlenton;
        }
        static T& GetMe()
        {
            return *Instance();
        }
        static void DelInstance()
        {
            DelMe();
        }
};
template<typename T,typename MANA>
T* Singleton<T,MANA>::singlenton = nullptr;


#endif //_SINGLENTON_H_

