/*
 * @brief
 * @auther aolei1024@gmail.com
 * @date 2020-06-09
 */
#ifndef _CHUNK_H_
#define _CHUNK_H_

#include <cstdint>
#include <array>
#include <tuple>
#include <vector>


namespace Temp
{
    using byte = uint8_t;
    struct alignas(128) Chunk
    {
        static constexpr size_t size = 16 * 1024;
        struct Layout
        {
            size_t capacity;
            std::vector<size_t> offsets;
        };
        static Layout GenLayout(const std::vector<size_t>& alignments, const std::vector<size_t>& sizes) noexcept;

        byte* Data() noexcept { return buffer.data(); }

        private:
        std::array<byte, size> buffer;
    };
}


#endif //_CHUNK_H_
