/*
 * @brief 一些测试需要的代码
 * @auther aolei1024@gmail.com
 * @date 2019-08-21
 */
#ifndef _BASE_TEST_H_
#define _BASE_TEST_H_

#include <sys/time.h>
#include <string>
#include <iostream>

#define RUNTIME  \
    BaseTest::RunTime _timet(__FUNCTION__);

namespace BaseTest
{
    class RunTime
    {
        public:
            RunTime(std::string fileName):name(fileName)
            {
                gettimeofday(&start,NULL);
            }
            ~RunTime()
            {
                struct timeval end;
                gettimeofday(&end,NULL);
                auto cost = end.tv_sec * 1000 + end.tv_usec/1000 - (start.tv_sec * 1000 + start.tv_usec / 1000);
                std::cout<<"RunTime\t function:"<<name<<"\t cost:"<<cost<<std::endl;
            }
        private:
            std::string name;
            struct timeval start;
    };
};


#endif //_BASE_TEST_H_
