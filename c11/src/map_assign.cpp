#include "map_assign.h"

#include <iostream>
#include <thread>
#include <chrono>

namespace Assign
{
    void Test::Func1(Test* ptr)
    {
        while(1)
        {
            {
                std::unique_lock<std::mutex> lk(ptr->lock);
                ptr->cv.wait(lk);
            }
            {

                std::lock_guard<std::mutex> lk(ptr->lock);
                std::cout<<ptr->i<<std::endl;
            }
        }
        std::cout<<"over"<<std::endl;
        std::this_thread::sleep_for(std::chrono::seconds(10));
//        ptr->cv.notify_all();
    }
    void Test::Func2(Test* ptr)
    {
        while(1)
        {
            {
                std::lock_guard<std::mutex> lk(ptr->lock);
                ptr->i++;
            }
            if(ptr->i % 5 == 0)
            {
                ptr->cv.notify_all();
 //               std::unique_lock<std::mutex> lk(ptr->lock);
  //              ptr->cv.wait(lk);
//              return;
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(1));
        }
    }

    void Test::Run()
    {
        std::thread t1(Test::Func1,this);
        std::thread t2(Test::Func2,this);
        t1.join();
        t2.join();
        std::cout<<"Run over"<<std::endl;
    }
}
